data "aws_route53_zone" "zone" {
  name = "${var.domain}."
}

resource "aws_route53_record" "root" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = var.domain
  type    = "A"
  ttl     = 300
  records = [aws_eip.static.public_ip]
}

resource "aws_route53_record" "files" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "files.${var.domain}"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.s3_files_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_files_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}
