resource "aws_ses_domain_identity" "mastodon_domain" {
  domain = var.domain
}

resource "aws_ses_domain_identity_verification" "domain_verification" {
  domain = aws_ses_domain_identity.mastodon_domain.id

  depends_on = [aws_route53_record.amazonses_verification_record]
}

resource "aws_ses_domain_dkim" "mastodon_domain" {
  domain = aws_ses_domain_identity.mastodon_domain.domain
}

resource "aws_ses_domain_mail_from" "mastodon_domain" {
  domain           = aws_ses_domain_identity.mastodon_domain.domain
  mail_from_domain = "mail.${aws_ses_domain_identity.mastodon_domain.domain}"
}

resource "aws_route53_record" "amazonses_verification_record" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "_amazonses.${var.domain}"
  type    = "TXT"
  ttl     = "300"
  records = [aws_ses_domain_identity.mastodon_domain.verification_token]
}

resource "aws_route53_record" "amazonses_dkim_record" {
  count   = 3
  zone_id = data.aws_route53_zone.zone.id
  name    = "${aws_ses_domain_dkim.mastodon_domain.dkim_tokens[count.index]}._domainkey"
  type    = "CNAME"
  ttl     = "1800"
  records = ["${aws_ses_domain_dkim.mastodon_domain.dkim_tokens[count.index]}.dkim.amazonses.com"]
}

resource "aws_route53_record" "ses_domain_mail_from_mx" {
  zone_id = data.aws_route53_zone.zone.id
  name    = aws_ses_domain_mail_from.mastodon_domain.mail_from_domain
  type    = "MX"
  ttl     = "300"
  records = ["10 feedback-smtp.${data.aws_region.current.name}.amazonses.com"]
}

resource "aws_route53_record" "ses_domain_mail_from_txt" {
  zone_id = data.aws_route53_zone.zone.id
  name    = aws_ses_domain_mail_from.mastodon_domain.mail_from_domain
  type    = "TXT"
  ttl     = "300"
  records = ["v=spf1 include:amazonses.com ~all"]
}

resource "aws_iam_user" "smtp" {
  name = "mastodon_smtp_user"
}

resource "aws_iam_user_policy" "smtp" {
  name = "AmazonSesSendingAccess"
  user = aws_iam_user.smtp.name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["ses:SendRawEmail", ]
        Effect = "Allow"
        "Resource" : "arn:aws:ses:${data.aws_region.current.name}:${data.aws_caller_identity.account.account_id}:identity/${var.domain}"
      },
    ]
  })
}

resource "aws_iam_access_key" "smtp" {
  user = aws_iam_user.smtp.name
}
