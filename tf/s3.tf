resource "aws_s3_bucket" "mastodon_files" {
  bucket = "files.${var.domain}"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "mastodon_files_bucket_encryption" {
  bucket = aws_s3_bucket.mastodon_files.id

  rule {
    bucket_key_enabled = true

    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "lifecycle" {
  bucket = aws_s3_bucket.mastodon_files.id

  rule {
    id = "transition-to-ia-one-zone"

    filter {}

    transition {
      days          = 30
      storage_class = "ONEZONE_IA"
    }

    status = "Enabled"
  }
}

resource "aws_s3_bucket_policy" "cloudfront_policy" {
  bucket = aws_s3_bucket.mastodon_files.id
  policy = templatefile("artifacts/s3-bucket-policy.json", {
    bucket_name  = "${aws_s3_bucket.mastodon_files.id}",
    account      = "${data.aws_caller_identity.account.account_id}",
    distribution = "${aws_cloudfront_distribution.s3_files_distribution.id}"
  })
}

resource "aws_s3_bucket_website_configuration" "files" {
  bucket = aws_s3_bucket.mastodon_files.bucket

  index_document {
    suffix = "index.html"
  }
}

resource "aws_iam_user" "mastodon_s3" {
  name = "mastodon-s3-files"
}

resource "aws_iam_user_policy" "mastodon_s3" {
  name = "mastodon-s3-policy"
  user = aws_iam_user.mastodon_s3.name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Deny",
        "Action" : "s3:ListBucket",
        "Resource" : "arn:aws:s3:::files.${var.domain}"
      },
      {
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : [
          "arn:aws:s3:::files.${var.domain}",
          "arn:aws:s3:::files.${var.domain}/*"
        ]
      }
    ]
  })
}

resource "aws_iam_access_key" "mastodon_s3" {
  user = aws_iam_user.mastodon_s3.name
}
