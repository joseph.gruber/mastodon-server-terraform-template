terraform {
  backend "http" {
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.41"
    }
  }

  required_version = ">= 1.3.5"
}

provider "aws" {
  default_tags {
    tags = var.default_tags
  }
}

data "aws_caller_identity" "account" {}

data "aws_region" "current" {}
