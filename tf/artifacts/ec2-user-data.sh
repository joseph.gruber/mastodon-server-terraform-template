#!/bin/bash

# Create swap file
dd if=/dev/zero of=/swapfile bs=128M count=${swap_space}
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
"/swapfile swap swap defaults 0 0" >> /etc/fstab

# Allow key-only SSH login
sed 's/PasswordAuthentication yes/PasswordAuthentication no/' -i /etc/ssh/sshd_config
systemctl restart ssh.service

# Add user account
USER_NAME=${username}
adduser --disabled-password --gecos "" $USER_NAME
passwd -u -d $USER_NAME
sudo -u $USER_NAME mkdir /home/$USER_NAME/.ssh
sudo -u $USER_NAME chmod 700 /home/$USER_NAME/.ssh
sudo -u $USER_NAME ssh-keygen -t rsa -b 2048 -N "" -f /home/$USER_NAME/.ssh/id_rsa
cat /home/$USER_NAME/.ssh/id_rsa.pub > /home/$USER_NAME/.ssh/authorized_keys
echo "${public_key}" >> /home/$USER_NAME/.ssh/authorized_keys
chmod 600 /home/$USER_NAME/.ssh/authorized_keys
chown $USER_NAME:$USER_NAME /home/$USER_NAME/.ssh/authorized_keys
adduser $USER_NAME sudo

# Update installed packages
apt update && apt upgrade -y

# Install fail2ban to block repeated login attempts
apt install -y fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sed 's/destemail = root@localhost/destemail = ${email}/' -i /etc/fail2ban/jail.local
sed 's/#mode   = normal/enabled = true/' -i /etc/fail2ban/jail.local
echo -e "\n[sshd-ddos]\nenabled = true\nport = 22" >> /etc/fail2ban/jail.local
systemctl restart fail2ban

# Update firewall rules to only allow SSH, HTTP, and HTTPS
ufw enable
ufw allow ssh
ufw allow http
ufw allow https

# Add Node.js package
curl -sL https://deb.nodesource.com/setup_16.x | bash -

# Add Postgres GPG key and repos
wget -O /usr/share/keyrings/postgresql.asc https://www.postgresql.org/media/keys/ACCC4CF8.asc
echo "deb [signed-by=/usr/share/keyrings/postgresql.asc] http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/postgresql.list

# Install system pre-requisites
apt install -y \
    jq curl wget gnupg apt-transport-https lsb-release ca-certificates \
    imagemagick ffmpeg libpq-dev libxml2-dev libxslt1-dev file git-core \
    g++ libprotobuf-dev protobuf-compiler pkg-config nodejs gcc autoconf \
    bison build-essential libssl-dev libyaml-dev libreadline6-dev \
    zlib1g-dev libncurses5-dev libffi-dev libgdbm-dev \
    nginx redis-server redis-tools postgresql postgresql-contrib \
    certbot python3-certbot-nginx libidn11-dev libicu-dev libjemalloc-dev awscli

# Configure Node.js
corepack enable
yarn set version classic

# Add web-push library
npm install web-push -g

tee /home/$USER_NAME/mastdon-install.sh <<EOF
# Install Ruby using rbenv
adduser --disabled-login --gecos "" mastodon
su - mastodon
cd ~ || return
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
bash
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install 3.0.4
rbenv global 3.0.4
gem install bundler --no-document
exit

# Install and configure Postgres
sudo - postgres
PG_PASSWORD=$(aws secretsmanager get-secret-value --region ${region} --secret-id mastodon-postgres --query SecretString --output text)
psql -c "CREATE USER mastodon CREATEDB;"
psql -c "ALTER USER postgres WITH PASSWORD '$PG_PASSWORD';"
exit

# Set up Mastodon
su - mastodon
cd ~ || return
git clone https://github.com/mastodon/mastodon.git live && cd live || return
git checkout "$(git tag -l | grep -v 'rc[0-9]*$' | sort -V | tail -n 1)"
bundle config deployment 'true'
bundle config without 'development test'
bundle install -j"$(getconf _NPROCESSORS_ONLN)"
yarn install --pure-lockfile
RAILS_ENV=production bundle exec rake mastodon:setup
exit

# Create TLS certificate using Let's Encrypt
systemctl reload nginx
certbot --nginx -d ${domain}

# Set up nginx
cp /home/mastodon/live/dist/nginx.conf /etc/nginx/sites-available/mastodon
ln -s /etc/nginx/sites-available/mastodon /etc/nginx/sites-enabled/mastodon
sed -i 's/example.com/${domain}/' /etc/nginx/sites-available/mastodon
systemctl reload nginx

# Setup system services
cp /home/mastodon/live/dist/mastodon-*.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now mastodon-web mastodon-sidekiq mastodon-streaming
EOF
