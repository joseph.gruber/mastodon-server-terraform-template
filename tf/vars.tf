variable "default_tags" {
  description = "Default AWS tags"
  type        = map(string)
  default = {
    project = "mastodon-server"
  }
}

variable "domain" {
  type        = string
  description = "Domain of the Mastodon instance (e.g. mstdn.social)"
}

variable "vpc_id" {
  type        = string
  description = "ID of VPC the instance will be hosted in"
}

variable "subnet_id" {
  type        = string
  description = "ID of the subnet the instance will be hosted in"
}

variable "instance_type" {
  type        = string
  description = "EC2 instance type for the Mastodon server"
}

variable "root_volume_size" {
  type        = string
  description = "Volume size of the EC2 root volume in GB"
}

variable "swap_space" {
  type        = number
  description = "Swap space in GB. Should not be bigger than total RAM of instance."
}

variable "public_key" {
  type        = string
  description = "Public key for the administrative user"
}

variable "secret_id" {
  sensitive   = true
  type        = string
  description = "ID (name) of the secret in Secrets Manager storing the postgres user password"
}

variable "username" {
  sensitive   = true
  type        = string
  description = "Username for the Ubuntu instance"
}

variable "email" {
  sensitive   = true
  type        = string
  description = "Email to send notifications to"
}
