output "s3_access_key" {
  value = aws_iam_access_key.mastodon_s3.id
}

output "s3_secret_key" {
  value     = aws_iam_access_key.mastodon_s3.secret
  sensitive = true
}

output "smtp_username" {
  value = aws_iam_access_key.smtp.id
}

output "smtp_password" {
  value     = aws_iam_access_key.smtp.ses_smtp_password_v4
  sensitive = true
}
