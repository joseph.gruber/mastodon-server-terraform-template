data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-arm64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_kms_alias" "ebs" {
  name = "alias/aws/ebs"
}

resource "aws_instance" "mastodon_server" {
  ami                     = data.aws_ami.ubuntu.id
  disable_api_stop        = true
  disable_api_termination = true
  iam_instance_profile    = aws_iam_instance_profile.mastodon_ec2_profile.name
  instance_type           = var.instance_type
  subnet_id               = var.subnet_id
  user_data = templatefile("artifacts/ec2-user-data.sh", {
    domain     = "${var.domain}"
    username   = "${var.username}"
    email      = "${var.email}"
    region     = "${data.aws_region.current.name}"
    public_key = "${var.public_key}"
    swap_space = "${floor((var.swap_space * 1024) / 128)}"
  })
  user_data_replace_on_change = false
  vpc_security_group_ids      = [aws_security_group.default.id]

  root_block_device {
    encrypted   = true
    kms_key_id  = data.aws_kms_alias.ebs.arn
    volume_size = var.root_volume_size
    volume_type = "gp3"
  }

  metadata_options {
    http_endpoint = "enabled"
    http_tokens   = "required"
  }

  lifecycle {
    ignore_changes = [root_block_device[0].kms_key_id]
  }
}

resource "aws_eip" "static" {
  instance = aws_instance.mastodon_server.id
}

resource "aws_security_group" "default" {
  name        = "mastodon-ssh-https-inbound"
  description = "Allow SSH and HTTPS inbound"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description      = "Outbound access"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_iam_role" "mastodon_ec2_role" {
  name = "mastodon-ec2-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "mastodon_ec2_role_policy" {
  name = "mastodon-ec2-role-policy"
  role = aws_iam_role.mastodon_ec2_role.id
  policy = templatefile("artifacts/ec2-role-policy.json", {
    region    = "${data.aws_region.current.name}",
    account   = "${data.aws_caller_identity.account.account_id}",
    secret_id = "${var.secret_id}"
  })
}

resource "aws_iam_instance_profile" "mastodon_ec2_profile" {
  name = "mastodon-instance-profile"
  role = aws_iam_role.mastodon_ec2_role.name
}
