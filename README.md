# Mastodon Server

## Gitlab Infrastructure

This project comes with a Gitlab pipeline that uses the Terraform template. No changes are required. The pipeline can be used for maintaining your Terraform state via Gitlab as well as automatically deploying any changes. If you do not plan to use Gitlab, you can delete the `.gitlab-ci.yml` file.

## AWS Infrastructure

All AWS infrastructure is deployed and managed using Terraform. The deployed infrastructure includes, but is not all encompassing:
- EC2 instance (for all Mastodon services)
- S3 bucket (for storing user content)
- Cloudfront distribution (for providing public access to user content)
- Public SSL/TLS ACM certificate (for providing SSL/TLS access to the Cloudfront distribution)
- SES SMTP user account (via IAM)
- SES domain identity (for sending emails from Mastodon such as password resets)
- Route 53 DNS records (for the domain hosting the Mastodon instance and associated services)
- IAM users and roles to support the various infrastructure and limit permissions

### Prerequisites

Before deploying your Mastodon instance, the following must be configure prior.

- Password for the Postgres `postgres` user account. This should be a strong, random password. It must be stored in AWS Secrets Manager.
- An existing VPC to deploy the resources into
- A Route 53 domain zone using the domain for the Mastodon instance (e.g. *mstdn.social*)

### Input Variables

Several variables are provided in vars.tf to customize your deployment. They include:

- `domain` - the domain of the Mastodon instance such as *mstdn.social*.
- `vpc_id` - the id of the VPC to deploy the resources into.
- `subnet_id` - the subnet of the VPC for the EC2 instance.
- `instance_type` - the size & type of the EC2 instance.
- `root_volume_size` - the size of the EC2 root volume. Defaults to `20` GB.
- `swap_space` - the size of the swap file in GB. Should not exceed the total memory of the instance.
- `public_key` - the SSH public key of the administrative user.
- `secret_id` - the ID of the Secrets Manager secret for the Postgres `postgres` user.
- `username` - username for administrative purposes of the EC2.
- `email` - email address of the administrator which notifications will be sent to.

## Notes

- I haven't figured out how to get sudo/su to change to a different user in the user data script. The user data script will automatically create a `mastdon-install.sh` script in the home directory of `username` which must be run to complete the installation.
- The IAM users for storing files in S3 and the SES SMTP authentication are automatically created but it is highly recommended to manually rotate the access key and secret. Or at least ensure your Terraform state file is highly secured.

## Scaling Recommendations

Note, all of these recommendations will result in increased spend.

- Create a separate Postgres RDS instance.
  - Preferably utilize Amazon Aurora for it's serverless features and scalability.
- Create a separate Amazon ElastiCache (Redis) instance.
- Create a separate Amazon OpenSearch (Elasticsearch) instance for full-text indexing (if enabled).
  - Preferably utilize OpenSearch Serverless.
- Create an auto-scaling EC2 group for the front-end Mastodon app. Place an elastic load balancer (ELB) in front.
